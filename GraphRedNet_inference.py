import argparse
import torch
import imageio
import skimage.transform
import torchvision

import torch.optim
import GraphRedNet_model_res50
from utils import utils
from utils.utils import load_ckpt

image_w = 640
image_h = 480
def inference(args, device):

    model = GraphRedNet_model_res50.GraphRedNet(pretrained=False)
    load_ckpt(model, None, args.last_ckpt, device)
    model.eval()
    model = model.to(device)

    image = imageio.imread(args.rgb)

    # Bi-linear
    image = skimage.transform.resize(image, (image_h, image_w), order=1,
                                     mode='reflect', preserve_range=True)

    image = image / 255
    image = torch.from_numpy(image).float()
    image = image.permute(2, 0, 1)

    image = torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                             std=[0.229, 0.224, 0.225])(image)

    image = image.to(device).unsqueeze(0)

    pred = model(image)

    output = utils.color_label(torch.max(pred, 1)[1] + 1)[0]

    imageio.imsave(args.output, output.cpu().numpy().transpose((1, 2, 0)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='RedNet Indoor Sementic Segmentation')
    parser.add_argument('-r', '--rgb', default=None, metavar='DIR',
                        help='path to image')
    parser.add_argument('-o', '--output', default=None, metavar='DIR',
                        help='path to output')
    parser.add_argument('--cuda', action='store_true', default=False,
                        help='enables CUDA training')
    parser.add_argument('--last-ckpt', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')

    args = parser.parse_args()
    device = torch.device("cuda:0" if args.cuda and torch.cuda.is_available() else "cpu")

    inference(args, device)
