import torch
from torch import nn
import math
import torch.utils.model_zoo as model_zoo
from utils import utils
from utils.utils import image_w, image_h


class GraphRedNet_Basic(nn.Module):
    def __init__(self):
        super(GraphRedNet_Basic, self).__init__()
        self.image_w = image_w
        self.image_h = image_h

    def _init_weight(self, m):
        if isinstance(m, nn.Conv2d):
            n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            m.weight.data.normal_(0, math.sqrt(2. / n))
            # torch.nn.init.xavier_normal_(m.weight)
        elif isinstance(m, nn.ConvTranspose2d):
            n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            m.weight.data.normal_(0, math.sqrt(2. / n))
        elif isinstance(m, nn.BatchNorm2d):
            m.weight.data.fill_(1)
            m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []

        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def _make_transpose(self, block, planes, blocks, stride=1):

        upsample = None
        if stride != 1:
            upsample = nn.Sequential(
                nn.ConvTranspose2d(self.inplanes, planes,
                                   kernel_size=2, stride=stride,
                                   padding=0, bias=False),
                nn.BatchNorm2d(planes),
            )
        elif self.inplanes != planes:
            upsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes),
            )

        layers = []

        for i in range(1, blocks):
            layers.append(block(self.inplanes, self.inplanes))

        layers.append(block(self.inplanes, planes, stride, upsample))
        self.inplanes = planes

        return nn.Sequential(*layers)

    def _load_resnet_pretrained(self, model_pth=None):
        if model_pth is None:
            pretrain_dict = model_zoo.load_url(utils.model_urls['resnet34'])
        else:
            pretrain_dict = torch.load(model_pth)
        model_dict = {}
        state_dict = self.state_dict()
        for k, v in pretrain_dict.items():
            if k in state_dict:
                model_dict[k] = v
        state_dict.update(model_dict)
        self.load_state_dict(state_dict)


def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)


class GraphLayer(nn.Module):

    def __init__(self, in_c, hid_c, h, w):
        super(GraphLayer, self).__init__()
        concat_c = hid_c * 2 + 4
        self.theta = nn.Sequential(
            nn.Conv2d(in_c + 2, in_c, 1, 1),
            nn.Conv2d(in_c, h * w, 1, 1)
        )
        self.g_inmap = nn.Conv2d(in_c, hid_c, 1, 1)
        self.outmap = nn.Sequential(
            nn.Conv2d(hid_c, in_c, 1, 1, bias=False),
            nn.BatchNorm2d(in_c),
        )
        self.graph_layer_pairwise = nn.Sequential(
            nn.Linear(concat_c, hid_c),
            nn.Linear(hid_c, hid_c)
        )
        self.softmax = nn.Softmax(-2)
        self.hid_c = hid_c
        self.h = h
        self.w = w
        self.concat_c = concat_c
        xv, yv = torch.meshgrid([torch.arange(h), torch.arange(w)])

        grid = torch.stack([xv, yv], 0).unsqueeze(0).float()
        self.grid_idx = nn.Parameter(grid)
        self.grid_idx.requires_grad = False
        # grid_idx: (1, 2, h, w)

        self.apply(self._init_weight)

    def forward(self, in_x):
        bs = in_x.size(0)

        expand_x = torch.cat([in_x, self.grid_idx.expand(bs, -1, -1, -1)], 1)
        # expand_x: (bs, in_c + 2, h, w)

        atten_mat = self.theta(expand_x)
        # atten_mat (bs, h*w, h, w)
        atten_mat = atten_mat.view(bs, self.h*self.w, -1).permute(0, 2, 1).unsqueeze(-1)
        # atten_mat (bs, h*w, h, w)

        attri = self.g_inmap(in_x)
        # attri: (bs, hid_c, h, w)

        expand_attri = torch.cat([attri, self.grid_idx.expand(bs, -1, -1, -1)], 1)
        # expand_attri: (bs, hid_c + 2, h, w)

        attri_mat = expand_attri.view(bs, self.hid_c + 2, self.h * self.w)
        attri_mat = attri_mat.unsqueeze(-1).expand(-1, -1, -1, self.h * self.w)
        # attri_mat: (bs, hid_c + 2, h*w, h*w)
        attri_mat = torch.cat([attri_mat.permute(0, 2, 3, 1),
                               attri_mat.permute(0, 3, 2, 1)], )
        # attri_mat: (bs, h*w, h*w, hid_c * 2 + 4)
        attri_mat = attri_mat.view(-1, self.concat_c)
        # attri_mat: (bs, h*w * h*w, hid_c * 2 + 4)

        attri_pairwise_mat = self.graph_layer_pairwise(attri_mat) \
            .view(-1, self.h * self.w, self.h * self.w, self.hid_c)
        # attri_att_mat: (bs, h*w, h*w, hid_c)

        new_attri = torch.sum(self.softmax(atten_mat) * attri_pairwise_mat, dim=2)
        # new_attri: (bs, h*w, hid_c)
        new_attri = new_attri.permute(0, 2, 1)
        # new_attri: (bs, hid_c, h*w)
        new_attri = new_attri.view(bs, self.hid_c, self.h, self.w)
        # new_attri: (bs, hid_c, h, w)

        out_attri = self.outmap(new_attri)
        # new_attri: (bs, in_c, h, w)

        out = in_x + out_attri

        return out

    def _init_weight(self, m):
        if isinstance(m, nn.Conv2d):
            n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            m.weight.data.normal_(0, math.sqrt(2. / n))
        elif isinstance(m, nn.Linear):
            torch.nn.init.xavier_normal_(m.weight)
        elif isinstance(m, nn.BatchNorm2d):
            m.weight.data.fill_(0)
            m.bias.data.zero_()


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class TransBasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, upsample=None):
        super(TransBasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, inplanes)
        self.bn1 = nn.BatchNorm2d(inplanes)
        self.relu = nn.ReLU(inplace=True)
        if upsample is not None and stride != 1:
            self.conv2 = nn.ConvTranspose2d(inplanes, planes,
                                            kernel_size=3, stride=stride, padding=1,
                                            output_padding=1, bias=False)
        else:
            self.conv2 = conv3x3(inplanes, planes, stride)
        self.bn2 = nn.BatchNorm2d(planes)
        self.upsample = upsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.upsample is not None:
            residual = self.upsample(x)

        out += residual
        out = self.relu(out)

        return out


class TransBottleneck(nn.Module):
    def __init__(self, inplanes, planes, stride=1, upsample=None, feature_scale=2):
        super(TransBottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, inplanes // feature_scale, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(inplanes // feature_scale)

        if upsample is not None and stride != 1:
            self.conv2 = nn.ConvTranspose2d(inplanes // feature_scale, inplanes // feature_scale,
                                            kernel_size=3, stride=stride, padding=1,
                                            output_padding=1, bias=False)
        else:
            self.conv2 = conv3x3(inplanes // feature_scale, inplanes // feature_scale)
        self.bn2 = nn.BatchNorm2d(inplanes // feature_scale)
        self.conv3 = nn.Conv2d(inplanes // feature_scale, planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.upsample = upsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.upsample is not None:
            residual = self.upsample(x)

        out += residual
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride,
                               padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out
