## Graph-RedNet Methods for Indoor Semantic Segmentation 
This is an individual project for indoor scene semantic segmentation.

### Model
This is the architecture of my model  
![Model Architecture](./figures/model.png)

### Result

#### sample results
Following is some sample results of my model

![sample results](./figures/result.png)

#### Quantitative Comparison

![comparasion result](./figures/comparasion.png)
 