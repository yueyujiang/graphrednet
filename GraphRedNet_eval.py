import argparse
import os
import numpy as np
import sys
import math

from torch.utils.data import DataLoader
import torch.optim
import torchvision.transforms as transforms
from torch.autograd import Variable
from utils.utils import load_ckpt

import GraphRedNet_model
import SUNRGBD_data
from utils import utils
import GraphRedNet_model_res34


def eval(last_ckpt, device, n_cpu, eval_batch_size, eval_mode):
    test_data = SUNRGBD_data.SUNRGBD(transform=transforms.Compose([SUNRGBD_data.scaleNorm(),
                                                                   SUNRGBD_data.ToTensor(),
                                                                   SUNRGBD_data.Normalize()]),
                                     phase_train=False,
                                     data_dir=args.data_dir)

    model = GraphRedNet_model.GraphRedNet(pretrained=False)

    model = model.to(device)
    load_ckpt(model, None, last_ckpt, device)
    model.eval()

    evaluation(model, test_data, None, n_cpu, eval_batch_size, device,
               eval_mode, None)


def evaluation(model, data, epoch, eval_worker, batch_size,
               device, mode='miou', writer=None, n_class=37):
    data_loader = DataLoader(data, batch_size=batch_size, shuffle=False,
                             num_workers=eval_worker, pin_memory=False)

    n_array = np.zeros(n_class)
    d_array = np.zeros(n_class)

    if mode == 'miou':
        def _metrics(pred_label, target):
            return _miou_raw(pred_label, target, n_class)
    elif mode == 'macc':
        def _metrics(pred_label, target):
            return _macc_raw(pred_label, target, n_class)
    else:
        def _metrics(pred_label, target):
            return _global_raw(pred_label, target, n_class)

    with torch.no_grad():
        for batch_idx, sample in enumerate(data_loader):
            image = sample['image'].to(device)
            target = sample['label'].to(device)
            pred = model(image)
            pred_label = torch.max(pred, 1)[1]

            pred_label = pred_label.clone().cpu().data.numpy()
            target = target.clone().cpu().data.numpy()

            for p, t in zip(pred_label, target):
                n_new, d_new = _metrics(p, t)
                n_array += n_new
                d_array += d_new

        if mode == 'global':
            acc = np.sum(n_array) / np.sum(d_array)
        else:
            acc = np.mean(n_array / d_array)

        if writer is not None:
            writer.add_scalar('Testing data evaluation', acc, global_step=int(epoch))
        print('Testing data evaluation {}: {:.4f}'.format(mode, acc))


# def _compute(lock, pred_labels, targets, n_class, n_array,
#              d_array, mode='miou'):
#     assert mode in ['macc', 'global', 'miou'], 'mode mush be one of miou or macc or global'
#     if mode == 'miou':
#         def _metrics(pred_label, target):
#             return _miou_raw(pred_label, target, n_class)
#     elif mode == 'macc':
#         def _metrics(pred_label, target):
#             return _macc_raw(pred_label, target, n_class)
#     else:
#         def _metrics(pred_label, target):
#             return _global_raw(pred_label, target, n_class)
#     n = np.zeros(n_class)
#     d = np.zeros(n_class)
#     for pred_label, target in zip(pred_labels, targets):
#         n_new, d_new = _metrics(pred_label, target)
#         n += n_new
#         d += d_new
#
#     with lock:
#         n_array += n
#         d_array += d


def _miou_raw(pred_label, target, n_class):
    mask = target != 0
    pred_label = pred_label[mask]
    target = target[mask] - 1
    n_new = np.zeros(n_class)
    d_new = np.zeros(n_class)
    for c in np.unique([pred_label, target]):
        pred_pixel_c = (pred_label == c).astype(np.int8)
        label_pixel_c = (target == c).astype(np.int8)
        intersection = np.sum((pred_pixel_c + label_pixel_c) == 2)
        total_c = np.sum(pred_pixel_c) + np.sum(label_pixel_c) - intersection
        n_new[int(c)] += intersection
        d_new[int(c)] += total_c
    return n_new, d_new


def _macc_raw(pred_label, target, n_class):
    mask = target != 0
    pred_label = pred_label[mask[:, :]]
    target = target[mask] - 1
    n_new = np.zeros(n_class)
    d_new = np.zeros(n_class)
    for c in np.unique([pred_label, target]):
        pred_pixel_c = (pred_label == c).astype(np.int8)
        label_pixel_c = (target == c).astype(np.int8)
        intersection = np.sum((pred_pixel_c + label_pixel_c) == 2)
        n_new[int(c)] += intersection
        d_new[int(c)] += np.sum(label_pixel_c)
    return n_new, d_new


def _global_raw(pred_label, target, n_class):
    mask = target != 0
    pred_label = pred_label[mask[:, :]]
    target = target[mask] - 1
    n_new = np.zeros(n_class)
    d_new = np.zeros(n_class)
    for c in np.unique([pred_label, target]):
        pred_pixel_c = (pred_label == c).astype(np.int8)
        label_pixel_c = (target == c).astype(np.int8)
        intersection = np.sum((pred_pixel_c + label_pixel_c) == 2)
        n_new[int(c)] += intersection
        d_new[int(c)] += np.sum(label_pixel_c)
    return n_new, d_new


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DLinkNet Indoor Sementic Segmentation')
    parser.add_argument('--data-dir', default=None, metavar='DIR',
                        help='path to SUNRGB-D')
    parser.add_argument('--meta-dir', default=None, metavar='DIR',
                        help='path to SUNRGBDMeta.mat')
    parser.add_argument('--split-dir', default=None, metavar='DIR',
                        help='path to allsplit.mat')
    parser.add_argument('--seg-dir', default=None, metavar='DIR',
                        help='path to SUNRGBD2Dseg.mat')
    parser.add_argument('--cuda', action='store_true', default=False,
                        help='enables CUDA training')
    parser.add_argument('--last-ckpt', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')
    parser.add_argument('--eval-worker', default=6, type=int, metavar='N',
                        help='number worker when evaluating (default: 6)')
    # parser.add_argument('--eval-batch-size', default=10, type=int, metavar='N',
    #                     help='Evaluation batch size (default: 15)')
    parser.add_argument('-m', '--model', default=None, metavar='MOD',
                        help='training model res34bottleneck24deconv or res34simplify12deconv or std')
    parser.add_argument('--eval-mode', default='miou', metavar='MOD',
                        help='eval mode, miou or macc or global')
    parser.add_argument('--eval-batch-size', default=10, type=int, metavar='N',
                        help='Evaluation batch size (default: 15)')

    args = parser.parse_args()
    device = torch.device("cuda:0" if args.cuda and torch.cuda.is_available() else "cpu")
    if args.model == 'res34standard32deconv':
        GraphRedNet_model = GraphRedNet_model_res34
        print('Using res34standard32deconv')
    elif args.model == 'res50standard32deconv':
        print('using res50standard32deconv')
    else:
        print('model name error')
        sys.exit()
    eval(args.last_ckpt, device, args.eval_worker, args.eval_batch_size, args.eval_mode)
