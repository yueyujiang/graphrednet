from torch import nn
from torch.utils.checkpoint import checkpoint
from GraphRedNet_model_basic import GraphRedNet_Basic, TransBasicBlock, Bottleneck

class GraphRedNet(GraphRedNet_Basic):
    def __init__(self, num_classes=37, pretrained=False, resnet_pretrain_dir=None):

        super(GraphRedNet, self).__init__()
        self.m = 'res50standard32deconv'
        block = Bottleneck
        transblock = TransBasicBlock
        layers = [3, 4, 6, 3]
        # original resnet
        self.inplanes = 64
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        # self.avgpool = nn.AvgPool2d(7)
        # self.fc = nn.Linear(512 * block.expansion, num_classes)

        # convolutional transpose layers
        self.inplanes = 512
        self.deconv1 = self._make_transpose(transblock, 256, 6, stride=2)
        self.deconv2 = self._make_transpose(transblock, 128, 4, stride=2)
        self.deconv3 = self._make_transpose(transblock, 64, 3, stride=2)
        self.deconv4 = self._make_transpose(transblock, 64, 3, stride=2)

        self.agant0 = self._make_agant_layer(64, 64)
        self.agant1 = self._make_agant_layer(64 * 4, 64)
        self.agant2 = self._make_agant_layer(128 * 4, 128)
        self.agant3 = self._make_agant_layer(256 * 4, 256)
        self.agant4 = self._make_agant_layer(512 * 4, 512)

        # final block
        self.inplanes = 64
        self.final_conv = self._make_transpose(transblock, 64, 3)

        self.final_deconv = nn.ConvTranspose2d(self.inplanes, num_classes, kernel_size=2,
                                               stride=2, padding=0, bias=True)

        self.out5_conv = nn.Conv2d(256, num_classes, kernel_size=1, stride=1, bias=True)
        self.out4_conv = nn.Conv2d(128, num_classes, kernel_size=1, stride=1, bias=True)
        self.out3_conv = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, bias=True)
        self.out2_conv = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, bias=True)

        self.apply(self._init_weight)
        if pretrained:
            self._load_resnet_pretrained(resnet_pretrain_dir)

    def forward_downsample(self, rgb):

        x = self.conv1(rgb)
        x = self.bn1(x)
        fuse0 = self.relu(x)

        x = self.maxpool(fuse0)

        # block 1
        fuse1 = self.layer1(x)
        # block 2
        fuse2 = self.layer2(fuse1)
        # block 3
        fuse3 = self.layer3(fuse2)
        # block 4
        fuse4 = self.layer4(fuse3)

        return fuse0, fuse1, fuse2, fuse3, fuse4

    def forward_upsample(self, fuse0, fuse1, fuse2, fuse3, fuse4):

        agant4 = self.agant4(fuse4)
        # upsample 1
        x = self.deconv1(agant4)
        if self.training:
            out5 = self.out5_conv(x)
        x = x + self.agant3(fuse3)
        # upsample 2
        x = self.deconv2(x)
        if self.training:
            out4 = self.out4_conv(x)
        x = x + self.agant2(fuse2)
        # upsample 3
        x = self.deconv3(x)
        if self.training:
            out3 = self.out3_conv(x)
        x = x + self.agant1(fuse1)
        # upsample 4
        x = self.deconv4(x)
        if self.training:
            out2 = self.out2_conv(x)
        x = x + self.agant0(fuse0)
        # final
        x = self.final_conv(x)
        out = self.final_deconv(x)

        if self.training:
            return out, out2, out3, out4, out5

        return out

    def forward(self, rgb, phase_checkpoint=False):

        if phase_checkpoint:
            fuses = checkpoint(self.forward_downsample, rgb)
            out = checkpoint(self.forward_upsample, *fuses)
        else:
            fuses = self.forward_downsample(rgb)
            out = self.forward_upsample(*fuses)

        return out