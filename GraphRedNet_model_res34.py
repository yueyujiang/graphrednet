from torch import nn
import torch
from torch.utils.checkpoint import checkpoint
from GraphRedNet_model_basic import GraphRedNet_Basic, \
    BasicBlock, TransBasicBlock, GraphLayer


class GraphRedNet(GraphRedNet_Basic):
    def __init__(self, num_classes=37, phase_graph = True,
                 pretrained=False, resnet_pretrain_dir=None,
                 loss_fn=None):

        super(GraphRedNet, self).__init__()
        self.m = 'res34standard32deconv'
        block = BasicBlock
        transblock = TransBasicBlock
        self.num_classes = num_classes
        self.phase_graph = phase_graph
        self.loss_fn = loss_fn
        layers = [3, 4, 6, 3]
        # original resnet
        self.inplanes = 64
        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        # self.avgpool = nn.AvgPool2d(7)
        # self.fc = nn.Linear(512 * block.expansion, num_classes)

        # convolutional transpose layers
        self.deconv1 = self._make_transpose(transblock, 256, 6, stride=2)
        self.deconv2 = self._make_transpose(transblock, 128, 4, stride=2)
        self.deconv3 = self._make_transpose(transblock, 64, 3, stride=2)
        self.deconv4 = self._make_transpose(transblock, 64, 3, stride=2)

        # final block
        self.final_conv = self._make_transpose(transblock, 64, 2)

        self.final_deconv = nn.ConvTranspose2d(self.inplanes, num_classes, kernel_size=2,
                                               stride=2, padding=0, bias=True)

        self.out5_conv = nn.Conv2d(256, num_classes, kernel_size=1, stride=1, bias=True)
        self.out4_conv = nn.Conv2d(128, num_classes, kernel_size=1, stride=1, bias=True)
        self.out3_conv = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, bias=True)
        self.out2_conv = nn.Conv2d(64, num_classes, kernel_size=1, stride=1, bias=True)

        self.apply(self._init_weight)
        if pretrained:
            self._load_resnet_pretrained(resnet_pretrain_dir)
        if phase_graph:
            self.graph_layer = GraphLayer(512, 128, self.image_h//32,
                                          self.image_w//32)

    def forward_downsample(self, rgb, phase_graph_update):
        x = self.conv1(rgb)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.maxpool(x)

        # block 1
        fuse1 = self.layer1(x)
        # block 2
        fuse2 = self.layer2(fuse1)
        # block 3
        fuse3 = self.layer3(fuse2)
        # block 4
        fuse4 = self.layer4(fuse3)
        # fuse4: (bs, 512, h/32, w/32)
        if self.phase_graph and phase_graph_update:
            fuse4 = self.graph_layer(fuse4)

        return fuse1, fuse2, fuse3, fuse4

    def forward_upsample(self, fuse1, fuse2, fuse3, fuse4, target):

        x = self.deconv1(fuse4)
        if self.training:
            out5 = self.out5_conv(x)
        x = x + fuse3
        # upsample 2
        x = self.deconv2(x)
        if self.training:
            out4 = self.out4_conv(x)
        x = x + fuse2
        # upsample 3
        x = self.deconv3(x)
        if self.training:
            out3 = self.out3_conv(x)
        x = x + fuse1
        # upsample 4
        x = self.deconv4(x)
        if self.training:
            out2 = self.out2_conv(x)
        # final
        x = self.final_conv(x)
        out = self.final_deconv(x)

        if self.training:
            loss = self.loss_fn((out, out2, out3, out4, out5), target)
            return out, loss.unsqueeze(0)

        return out

    def forward(self, rgb, phase_checkpoint=False, target=None, phase_graph_update=False):

        if phase_checkpoint:
            fuses = checkpoint(self.forward_downsample, rgb, phase_graph_update)
            fuses = fuses + (target,)
            out = checkpoint(self.forward_upsample, *fuses)
        else:
            fuses = self.forward_downsample(rgb, phase_graph_update)
            fuses = fuses + (target,)
            out = self.forward_upsample(*fuses)

        return out
