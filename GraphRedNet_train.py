import argparse
import sys
import os
import time
import torch

from torch.utils.data import DataLoader
import torch.optim
import torchvision.transforms as transforms
from torchvision.utils import make_grid
from torch import nn

from tensorboardX import SummaryWriter

import GraphRedNet_model
import GraphRedNet_model_res34
import GraphRedNet_model_res50
import GraphRedNet_model_res101
import SUNRGBD_data
from GraphRedNet_eval import evaluation
from utils import utils
from utils.utils import save_ckpt
from utils.utils import load_ckpt, load_from_nograph
from utils.utils import print_log
from torch.optim.lr_scheduler import LambdaLR
from utils.utils import image_w, image_h

num_epoch_per_new_writer = 30


def train(args, device):
    train_data = SUNRGBD_data.SUNRGBD(
        transform=transforms.Compose(
            [SUNRGBD_data.scaleNorm(),
             SUNRGBD_data.RandomScale((1.0, 1.4)),
             SUNRGBD_data.RandomHSV((0.9, 1.1),
                                    (0.9, 1.1),
                                    (25, 25)),
             SUNRGBD_data.RandomCrop(image_h, image_w),
             SUNRGBD_data.RandomFlip(),
             SUNRGBD_data.ToTensor(),
             SUNRGBD_data.Normalize()]
        ),
        phase_train=True,
        data_dir=args.data_dir
    )
    train_loader = DataLoader(train_data, batch_size=args.batch_size, shuffle=True,
                              num_workers=args.workers, pin_memory=False)
    if args.epochs_per_eval:
        test_data = SUNRGBD_data.SUNRGBD(
            transform=transforms.Compose(
                [SUNRGBD_data.scaleNorm(),
                 SUNRGBD_data.ToTensor(),
                 SUNRGBD_data.Normalize()]
            ),
            phase_train=False,
            data_dir=args.data_dir
        )
        num_test = len(test_data)

    num_train = len(train_data)

    CEL_weighted = utils.CrossEntropyLoss2d()
    if args.last_ckpt:
        model = GraphRedNet_model.GraphRedNet(pretrained=False,
                                              phase_graph=not args.nograph,
                                              loss_fn=CEL_weighted)
    else:
        model = GraphRedNet_model.GraphRedNet(pretrained=True,
                                              phase_graph=not args.nograph,
                                              resnet_pretrain_dir=args.resnet_dir,
                                              loss_fn=CEL_weighted)
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        model = nn.DataParallel(model)
        model.m = args.model
    model = model.to(device)
    model.train()

    # CEL_weighted = CEL_weighted.to(device)
    optimizer_other = None
    if not args.nograph:
        param = [p for n, p in model.named_parameters() if 'graph_layer' not in n and p.requires_grad]

        optimizer_other = torch.optim.SGD(param,
                                          lr=args.lr, momentum=args.momentum,
                                          weight_decay=args.weight_decay)
    optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad,
                                       model.parameters()),
                                lr=args.lr, momentum=args.momentum,
                                weight_decay=args.weight_decay)

    phase_graph_update = False

    global_step = 0

    if args.last_ckpt:
        if args.load_from_nograph:
            global_step, args.start_epoch = load_from_nograph(model, args.last_ckpt, device)
            args.nograph = False
        else:
            global_step, args.start_epoch, phase_nograph, phase_graph_update = \
                load_ckpt(model, optimizer, optimizer_other, args.last_ckpt, device)
            assert phase_nograph == args.nograph, 'args.nograph should consistant ' \
                                                  'with loaded phase_nograph'

    lr_decay_lambda = lambda epoch: args.lr_decay_rate ** (epoch // args.lr_epoch_per_decay)
    scheduler = LambdaLR(optimizer, lr_lambda=lr_decay_lambda)

    for epoch in range(int(args.start_epoch), args.epochs):

        # adjust_learning_rate(optimizer, epoch, args.lr,
        #                      args.lr_decay_rate, args.lr_epoch_per_decay)
        scheduler.step(epoch)
        local_count = 0
        last_count = 0
        end_time = time.time()
        # need new write from time to time in case file too big to process
        if (args.last_ckpt and epoch == args.start_epoch) or epoch % num_epoch_per_new_writer == 0:
            writer = SummaryWriter(args.summary_dir)
            # if epoch == 0:
            #     if args.graph_summary:
            #         writer.add_graph(model, (torch.cat([train_data[i]['image'] for i in range(5)]).to(device),
            #                                  torch.cat([train_data[i]['depth'] for i in range(5)]).to(device)))
        # if device.type == 'cuda':
        #     torch.cuda.empty_cache()
        # The begin of check point need no eval
        if (not args.last_ckpt and epoch != 0) or (args.last_ckpt and epoch != args.start_epoch):
            if args.epochs_per_eval:
                if epoch % args.epochs_per_eval == 0:
                    model.eval()
                    evaluation(model, test_data, epoch, args.eval_worker, args.batch_size, device,
                               'miou', writer)
                    model.train()
                    # if device.type == 'cuda':
                    #     torch.cuda.empty_cache()

            if epoch % args.save_epoch_freq == 0:
                if phase_graph_update or args.nograph:
                    save_ckpt(args.ckpt_dir, model, optimizer, global_step, epoch,
                              local_count, args.batch_size, num_train, args.nograph, phase_graph_update)
                else:
                    save_ckpt(args.ckpt_dir, model, optimizer_other, global_step, epoch,
                              local_count, args.batch_size, num_train, args.nograph, phase_graph_update)
        for batch_idx, sample in enumerate(train_loader):

            image = sample['image'].to(device)
            target_scales = [sample[s].to(device) for s in ['label', 'label2', 'label3', 'label4', 'label5']]
            if phase_graph_update or args.nograph:
                optimizer.zero_grad()
            else:
                optimizer_other.zero_grad()
            pred, loss_gather = model(image, args.checkpoint,
                                      target_scales, phase_graph_update)
            loss = torch.mean(loss_gather, 0)
            loss.backward()

            if phase_graph_update or args.nograph:
                optimizer.step()
            else:
                optimizer_other.step()
            if not phase_graph_update and loss < args.graph_update_threshold:
                phase_graph_update = True
            local_count += image.data.shape[0]
            global_step += 1
            if global_step % args.print_freq == 0 or global_step == 1:

                time_inter = time.time() - end_time
                count_inter = local_count - last_count
                print_log(global_step, epoch, local_count, count_inter,
                          num_train, loss, time_inter)
                end_time = time.time()

                for name, param in model.named_parameters():
                    writer.add_histogram(name, param.cpu().detach().numpy(), global_step)
                grid_image = make_grid(image[:6].cpu().detach(), 3, normalize=True)
                writer.add_image('image', grid_image, global_step)
                grid_image = make_grid(utils.color_label(torch.max(pred[:6], 1)[1] + 1), 3, normalize=False,
                                       range=(0, 255))
                writer.add_image('Predicted label', grid_image, global_step)
                grid_image = make_grid(utils.color_label(target_scales[0][:6]), 3, normalize=False, range=(0, 255))
                writer.add_image('Groundtruth label', grid_image, global_step)
                writer.add_scalar('CrossEntropyLoss', loss.data, global_step=global_step)
                writer.add_scalar('Learning rate', scheduler.get_lr()[0], global_step=global_step)
                writer.add_scalar('phase_graph_update', 1 if phase_graph_update else 0, global_step=global_step)
                last_count = local_count

    if phase_graph_update or args.nograph:
        save_ckpt(args.ckpt_dir, model, optimizer, global_step, args.epochs,
                  0, args.batch_size, num_train, args.nograph, phase_graph_update)
    else:
        save_ckpt(args.ckpt_dir, model, optimizer_other, global_step, args.epochs,
                  0, args.batch_size, num_train, args.nograph, phase_graph_update)

    model.eval()
    evaluation(model, test_data, args.epochs, args.eval_worker, args.batch_size, args.cuda,
               'miou', writer)

    print("Training completed ")


# def adjust_learning_rate(optimizer, epoch, init_lr, lr_decay_rate, lr_epoch_per_decay):
#     """Sets the learning rate to the initial LR decayed by lr_decay_rate every lr_epoch_per_decay epochs"""
#     lr = init_lr * (lr_decay_rate ** (epoch // lr_epoch_per_decay))
#     for param_group in optimizer.param_groups:
#         param_group['lr'] = lr


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='DLinkNet Indoor Sementic Segmentation')
    parser.add_argument('--data-dir', default=None, metavar='DIR',
                        help='path to SUNRGB-D')
    parser.add_argument('--resnet-dir', default=None, metavar='DIR',
                        help='path to resnet pretrain model')
    parser.add_argument('--nocuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('-j', '--workers', default=8, type=int, metavar='N',
                        help='number of data loading workers (default: 8)')
    parser.add_argument('--epochs', default=1500, type=int, metavar='N',
                        help='number of total epochs to run (default: 1500)')
    parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                        help='manual epoch number (useful on restarts)')
    parser.add_argument('-b', '--batch-size', default=16, type=int,
                        metavar='N', help='mini-batch size (default: 16)')
    parser.add_argument('--lr', '--learning-rate', default=2e-3, type=float,
                        metavar='LR', help='initial learning rate')
    parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float,
                        metavar='W', help='weight decay (default: 1e-4)')
    parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                        help='momentum')
    parser.add_argument('--print-freq', '-p', default=100, type=int,
                        metavar='N', help='print batch frequency (default: 100)')
    parser.add_argument('--save-epoch-freq', '-s', default=10, type=int,
                        metavar='N', help='save epoch frequency (default: 10)')
    parser.add_argument('--last-ckpt', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')
    parser.add_argument('--lr-decay-rate', default=0.8, type=float,
                        help='decay rate of learning rate (default: 0.8)')
    parser.add_argument('--lr-epoch-per-decay', default=200, type=int,
                        help='epoch of per decay of learning rate (default: 200)')
    parser.add_argument('--ckpt-dir', default='./model/', metavar='DIR',
                        help='path to save checkpoints')
    parser.add_argument('--summary-dir', default='./summary', metavar='DIR',
                        help='path to save summary')
    parser.add_argument('--epochs-per-eval', default=10, type=int,
                        metavar='N', help='Interval of epochs for a testing data evaluation.')
    parser.add_argument('--eval-worker', default=8, type=int, metavar='N',
                        help='number worker when evaluating (default: 8)')
    parser.add_argument('-m', '--model', default='res34standard32deconv', metavar='MOD',
                        help='training model')
    parser.add_argument('--checkpoint', action='store_true', default=False,
                        help='Using Pytorch checkpoint or not')
    parser.add_argument('--nograph', action='store_true', default=False,
                        help='disable graph layer in model')
    parser.add_argument('--graph-update-threshold', default=0.20, type=float,
                        help='Begin update graph under this threshold.')
    parser.add_argument('--load-from-nograph', action='store_true', default=False,
                        help='load weight from nograph structure')
    # parser.add_argument('--graph-summary', action='store_true', default=False,
    #                     help='Add graph at tensorboard or not, default no.')

    args = parser.parse_args()
    device = torch.device("cuda:0" if not args.nocuda and torch.cuda.is_available() else "cpu")

    if not os.path.exists(args.ckpt_dir):
        os.mkdir(args.ckpt_dir)
    if not os.path.exists(args.summary_dir):
        os.mkdir(args.summary_dir)
    if args.model == 'res34standard32deconv':
        GraphRedNet_model = GraphRedNet_model_res34
        print('Using res34standard32deconv')
    elif args.model == 'res50standard32deconv':
        GraphRedNet_model = GraphRedNet_model_res50
        print('Using res50standard32deconv')
    elif args.model == 'res101standard32deconv':
        GraphRedNet_model = GraphRedNet_model_res101
        print('Using res101standard32deconv')
    else:
        print('model name error')
        sys.exit()
    train(args, device)
