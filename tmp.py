import numpy as np
import scipy.io
import scipy.misc
import h5py
import os


data_dir = '/media/Datadrive/datasets/SUNRGB-D'
SUNRGBDMeta_dir = '/media/Datadrive/datasets/SUNRGB-D/SUNRGBDtoolbox/Metadata/SUNRGBDMeta.mat'
allsplit_dir = '/media/Datadrive/datasets/SUNRGB-D/SUNRGBDtoolbox/traintestSUNRGBD/allsplit.mat'
SUNRGBD2Dseg_dir = '/media/Datadrive/datasets/SUNRGB-D/SUNRGBDtoolbox/Metadata/SUNRGBD2Dseg.mat'
SUNRGBD2Dseg = h5py.File(SUNRGBD2Dseg_dir, mode='r', libver='latest')

SUNRGBDMeta = scipy.io.loadmat(SUNRGBDMeta_dir, squeeze_me=True,
                               struct_as_record=False)['SUNRGBDMeta']
alldepth = []
for i, meta in enumerate(SUNRGBDMeta):
    meta_dir = '/'.join(meta.rgbpath.split('/')[:-2])
    real_dir = meta_dir.replace('/n/fs/sun3d/data', data_dir)
    depth_bfx_path = os.path.join(real_dir, 'depth_bfx/' + meta.depthname)
    depth_bfx = scipy.misc.imread(depth_bfx_path)
    alldepth = np.append(alldepth, depth_bfx[:])
    if i >= 50:
        break

print(np.mean(alldepth[:]))
print(np.std(alldepth[:]))
